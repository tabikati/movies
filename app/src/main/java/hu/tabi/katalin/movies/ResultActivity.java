package hu.tabi.katalin.movies;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import hu.tabi.katalin.movies.fragment.TopListFragment;
import hu.tabi.katalin.movies.model.Item;
import hu.tabi.katalin.movies.model.Movie;
import hu.tabi.katalin.movies.model.Person;
import hu.tabi.katalin.movies.model.TvShow;
import hu.tabi.katalin.movies.util.AnimationHelper;
import hu.tabi.katalin.movies.util.Category;
import hu.tabi.katalin.movies.util.ConnectionHelper;
import hu.tabi.katalin.movies.util.Members;

public class ResultActivity extends FragmentActivity {


    public static final String EXTRA_CATEGORY = "category";
    public static final String EXTRA_QUERY = "query";

    private int mCategory;
    private String mQuery;

    private FrameLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mCategory = getIntent().getIntExtra(EXTRA_CATEGORY, 0);
        mQuery = getIntent().getStringExtra(EXTRA_QUERY);

        mContainer = (FrameLayout) findViewById(R.id.fragment_container);
        mContainer.setAlpha(0);

        switch (mCategory) {
            case Category.PAGE_MOVIES:
                searchMovies();
                break;
            case Category.PAGE_PEOPLE:
                searchPeople();
                break;
            case Category.PAGE_TVSHOWS:
                searchTvShows();
                break;
        }

    }

    private void searchMovies() {
        ConnectionHelper.searchMovie(this, mQuery, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Movie> movieList = Movie.parseMovieList(jsonObject);
                if (movieList != null && !movieList.isEmpty()) {
                    showResult(movieList);
                } else {
                    Toast.makeText(ResultActivity.this, getString(R.string.msg_not_found), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }

    private void searchPeople() {
        ConnectionHelper.searchPerson(this, mQuery, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Person> personList = Person.parsePersonList(jsonObject);
                if (personList != null && !personList.isEmpty()) {
                    downloadPeopleDetails(personList);
                } else {
                    Toast.makeText(ResultActivity.this, getString(R.string.msg_not_found), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onError() {
                finish();
            }
        });

    }

    private int mItemDownloaded;

    private void downloadPeopleDetails(final List<Person> personList) {
        for (final Person person : personList) {
            ConnectionHelper.getPersonDetails(ResultActivity.this, person.getId(), new ConnectionHelper.FinishListener() {
                @Override
                public void onSuccess(JsonObject jsonObject) {
                    person.setDesc(new Gson().fromJson(jsonObject.get(Members.BIO), String.class));
                    mItemDownloaded++;
                    if (mItemDownloaded == personList.size()) {
                        showResult(personList);
                    }
                }

                @Override
                public void onError() {
                    mItemDownloaded++;
                    if (mItemDownloaded == personList.size()) {
                        showResult(personList);
                    }
                }
            });
        }
    }

    private void searchTvShows() {
        ConnectionHelper.searchTvShow(this, mQuery, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<TvShow> tvShowList = TvShow.parseTvShowList(jsonObject);
                if (tvShowList != null && !tvShowList.isEmpty()) {
                    showResult(tvShowList);
                } else {
                    Toast.makeText(ResultActivity.this, getString(R.string.msg_not_found), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }

    private void showResult(List<? extends Item> items) {
        TopListFragment fragment = TopListFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, fragment).commit();
        fragment.update(items);
        AnimationHelper.animateShow(mContainer);
    }


}
