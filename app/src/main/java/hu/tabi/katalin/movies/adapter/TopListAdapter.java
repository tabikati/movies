package hu.tabi.katalin.movies.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import hu.tabi.katalin.movies.DetailsActivity;
import hu.tabi.katalin.movies.R;
import hu.tabi.katalin.movies.model.Item;
import hu.tabi.katalin.movies.model.Movie;
import hu.tabi.katalin.movies.model.Person;
import hu.tabi.katalin.movies.model.TvShow;
import hu.tabi.katalin.movies.util.Category;
import hu.tabi.katalin.movies.util.ConnectionHelper;
import hu.tabi.katalin.movies.util.Content;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class TopListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<? extends Item> mItemList;
    private LayoutInflater mLayoutInflater;

    public TopListAdapter(Context context, List<? extends Item> itemList) {
        mContext = context;
        mItemList = itemList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void update(List<? extends Item> itemList) {
        mItemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(mLayoutInflater.inflate(R.layout.item_movie, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case Category.PAGE_MOVIES:
                bindMovie((ItemHolder) holder, (Movie) mItemList.get(position));
                break;
            case Category.PAGE_PEOPLE:
                bindPerson((ItemHolder) holder, (Person) mItemList.get(position));
                break;
            case Category.PAGE_TVSHOWS:
                bindTv((ItemHolder) holder, (TvShow) mItemList.get(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mItemList.get(position) instanceof Movie) {
            return Category.PAGE_MOVIES;
        } else if (mItemList.get(position) instanceof Person) {
            return Category.PAGE_PEOPLE;
        } else if (mItemList.get(position) instanceof TvShow) {
            return Category.PAGE_TVSHOWS;
        }
        return super.getItemViewType(position);
    }

    private class ItemHolder extends RecyclerView.ViewHolder {

        ImageView posterImageView;
        TextView titleTextView;
        TextView genreTextView;
        TextView voteTextView;
        TextView dateTextView;
        TextView overviewTextView;
        TextView moreTextView;
        LinearLayout infoLayout;

        ItemHolder(View itemView) {
            super(itemView);
            posterImageView = (ImageView) itemView.findViewById(R.id.posterImageView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            genreTextView = (TextView) itemView.findViewById(R.id.genreTextView);
            voteTextView = (TextView) itemView.findViewById(R.id.voteTextView);
            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            overviewTextView = (TextView) itemView.findViewById(R.id.overviewTextView);
            moreTextView = (TextView) itemView.findViewById(R.id.moreTextView);
            infoLayout = (LinearLayout) itemView.findViewById(R.id.infoLayout);
        }
    }

    private void bindMovie(ItemHolder holder, final Movie movie) {
        holder.infoLayout.setVisibility(View.VISIBLE);
        holder.moreTextView.setVisibility(View.VISIBLE);
        holder.overviewTextView.setMaxLines(4);
        ConnectionHelper.loadImage(holder.posterImageView, movie.getPosterPath());
        holder.titleTextView.setText(movie.getTitle());
        String genres = "";
        for (int i = 0; i < movie.getGenreIds().length; i++) {
            String genre = Content.getInstance().getMovieGenre(movie.getGenreIds()[i]);
            if (genre != null) {
                if (genres.length() > 0) {
                    genres += ", ";
                }
                genres += genre;
            }
        }
        holder.genreTextView.setText(genres);
        holder.voteTextView.setText(movie.getVoteAverage() + "");
        if (movie.getReleaseDate() != null) {
            holder.dateTextView.setText(new SimpleDateFormat("yyyy", Locale.getDefault()).format(movie.getReleaseDate()));
        }
        holder.overviewTextView.setText(movie.getOverview());
        holder.moreTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDetailsActivity(Category.PAGE_MOVIES, movie.getId());
            }
        });
    }

    private void bindPerson(ItemHolder holder, Person person) {
        ConnectionHelper.loadImage(holder.posterImageView, person.getImage());
        holder.titleTextView.setText(person.getTitle());
        holder.overviewTextView.setText(person.getDesc());
        holder.infoLayout.setVisibility(View.GONE);
        holder.moreTextView.setVisibility(View.GONE);
        holder.overviewTextView.setMaxLines(8);
    }

    private void bindTv(ItemHolder holder, final TvShow tvShow) {
        holder.infoLayout.setVisibility(View.VISIBLE);
        holder.moreTextView.setVisibility(View.VISIBLE);
        holder.overviewTextView.setMaxLines(4);
        ConnectionHelper.loadImage(holder.posterImageView, tvShow.getPosterPath());
        holder.titleTextView.setText(tvShow.getTitle());
        String genres = "";
        for (int i = 0; i < tvShow.getGenreIds().length; i++) {
            String tvGenre = Content.getInstance().getTvGenre(tvShow.getGenreIds()[i]);
            if (tvGenre != null) {
                if (genres.length() > 0) {
                    genres += ", ";
                }
                genres += tvGenre;
            }
        }
        holder.genreTextView.setText(genres);
        holder.voteTextView.setText(tvShow.getVoteAverage() + "");
        if (tvShow.getReleaseDate() != null) {
            holder.dateTextView.setText(new SimpleDateFormat("yyyy", Locale.getDefault()).format(tvShow.getReleaseDate()));
        }
        holder.overviewTextView.setText(tvShow.getOverview());
        holder.moreTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDetailsActivity(Category.PAGE_TVSHOWS, tvShow.getId());
            }
        });
    }

    private void startDetailsActivity(int category, int id) {
        Intent intent = new Intent(mContext, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_CATEGORY, category);
        intent.putExtra(DetailsActivity.EXTRA_ID, id);
        mContext.startActivity(intent);
    }

}