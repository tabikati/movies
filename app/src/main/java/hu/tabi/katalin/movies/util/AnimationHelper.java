package hu.tabi.katalin.movies.util;

import android.view.View;

/**
 * Created by Kati on 2016. 10. 18..
 */

public class AnimationHelper {

    private static final int TRANSLATION_ANIMATION_DURATION = 300;
    private static final int ALPHA_ANIMATION_DURATION = 1000;


    public static void animateShow(View view) {
        view.animate().alpha(1).setDuration(ALPHA_ANIMATION_DURATION);
    }

    public static void animateDown(View view) {
        view.animate().translationY(view.getHeight()).setDuration(TRANSLATION_ANIMATION_DURATION);
    }

    public static void animateUp(View view) {
        view.animate().translationY(0).setDuration(TRANSLATION_ANIMATION_DURATION);
    }
}
