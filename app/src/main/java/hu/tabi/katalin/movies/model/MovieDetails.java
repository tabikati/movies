package hu.tabi.katalin.movies.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class MovieDetails extends Movie {

    @SerializedName("genres")
    private List<Genre> genreList;

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    public static MovieDetails parse(JsonObject jsonObject) {
        return getGson().fromJson(jsonObject, MovieDetails.class);
    }
}
