package hu.tabi.katalin.movies.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class Item {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    private static Gson mGson;

    public static Gson getGson() {
        if (mGson == null) {
            mGson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

                @Override
                public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                        throws JsonParseException {
                    try {
                        return df.parse(json.getAsString());
                    } catch (ParseException e) {
                        return null;
                    }
                }
            }).create();
        }
        return mGson;
    }

}
