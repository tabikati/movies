package hu.tabi.katalin.movies.util;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class CustomUtil {

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
