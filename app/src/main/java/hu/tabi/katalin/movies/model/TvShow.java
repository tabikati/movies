package hu.tabi.katalin.movies.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

import hu.tabi.katalin.movies.util.Members;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class TvShow extends Item {

    @SerializedName("id")
    private int mId;

    @SerializedName("poster_path")
    private String mPosterPath;

    @SerializedName("overview")
    private String mOverview;

    @SerializedName("first_air_date")
    private Date mReleaseDate;

    @SerializedName("genre_ids")
    private int[] mGenreIds;

    @SerializedName("name")
    private String mTitle;

    @SerializedName("vote_average")
    private float mVoteAverage;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String mPosterPath) {
        this.mPosterPath = mPosterPath;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String mOverview) {
        this.mOverview = mOverview;
    }

    public Date getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(Date mReleaseDate) {
        this.mReleaseDate = mReleaseDate;
    }

    public int[] getGenreIds() {
        return mGenreIds;
    }

    public void setGenreIds(int[] mGenreIds) {
        this.mGenreIds = mGenreIds;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public float getVoteAverage() {
        return mVoteAverage;
    }

    public void setVoteAverage(float mVoteAverage) {
        this.mVoteAverage = mVoteAverage;
    }

    public static List<TvShow> parseTvShowList(JsonObject jsonObject) {
        List<TvShow> list = null;
        try {
            list = getGson().fromJson(jsonObject.getAsJsonArray(Members.RESULTS), new TypeToken<List<TvShow>>() {
            }.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return list;
    }
}
