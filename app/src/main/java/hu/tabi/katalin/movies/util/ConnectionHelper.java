package hu.tabi.katalin.movies.util;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import hu.tabi.katalin.movies.R;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class ConnectionHelper {

    public interface FinishListener {
        void onSuccess(JsonObject jsonObject);

        void onError();
    }

    private static final String BASE_URL = "https://api.themoviedb.org/3";
    private static final String URL_POPULAR_MOVIES = BASE_URL + "/movie/popular";
    private static final String URL_POPULAR_PEOPLE = BASE_URL + "/person/popular";
    private static final String URL_POPULAR_TVSHOWS = BASE_URL + "/tv/popular";
    private static final String URL_MOVIE_GENRE_LIST = BASE_URL + "/genre/movie/list";
    private static final String URL_TV_GENRE_LIST = BASE_URL + "/genre/tv/list";
    private static final String URL_PERSON_DETAILS = BASE_URL + "/person";
    private static final String URL_MOVIE_DETAILS = BASE_URL + "/movie";
    private static final String URL_TV_DETAILS = BASE_URL + "/tv";

    private static final String URL_SEARCH_MOVIE = BASE_URL + "/search/movie";
    private static final String URL_SEARCH_PERSON = BASE_URL + "/search/person";
    private static final String URL_SEARCH_TV = BASE_URL + "/search/tv";

    private static final String URL_IMAGE = "https://image.tmdb.org/t/p/w500";

    private static final String API_KEY_NAME = "api_key";
    private static final String API_KEY_VALUE = "0a08e38b874d0aa2d426ffc04357069d";
    private static final String SEARCH_QUERY = "query";


    public static void getPopularMovies(Context context, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_POPULAR_MOVIES)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getMovieGenres(Context context, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_MOVIE_GENRE_LIST)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getPopularPeople(Context context, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_POPULAR_PEOPLE)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getPersonDetails(Context context, int personId, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_PERSON_DETAILS + "/" + personId)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getMovieDetails(Context context, int movieId, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_MOVIE_DETAILS + "/" + movieId)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getTvDetails(Context context, int tvId, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_TV_DETAILS + "/" + tvId)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getPopulatTvShows(Context context, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_POPULAR_TVSHOWS)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void getTvGenres(Context context, FinishListener finishListener) {
        Ion.with(context)
                .load(URL_TV_GENRE_LIST)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void loadImage(ImageView imageView, String path) {
        Ion.with(imageView)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .load(URL_IMAGE + path);
    }

    public static void searchMovie(final Context context, String query, final FinishListener finishListener) {
        Ion.with(context)
                .load(URL_SEARCH_MOVIE)
                .addQuery(SEARCH_QUERY, query)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void searchPerson(final Context context, String query, final FinishListener finishListener) {
        Ion.with(context)
                .load(URL_SEARCH_PERSON)
                .addQuery(SEARCH_QUERY, query)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    public static void searchTvShow(final Context context, String query, final FinishListener finishListener) {
        Ion.with(context)
                .load(URL_SEARCH_TV)
                .addQuery(SEARCH_QUERY, query)
                .addQuery(API_KEY_NAME, API_KEY_VALUE)
                .asJsonObject()
                .setCallback(getFutureCallback(context, finishListener));
    }

    private static FutureCallback<JsonObject> getFutureCallback(final Context context, final FinishListener finishListener) {
        return new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null && result != null) {
                    if (finishListener != null) {
                        finishListener.onSuccess(result);
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.error_downloading_content), Toast.LENGTH_SHORT).show();
                    if (finishListener != null) {
                        finishListener.onError();
                    }
                }
            }
        };
    }

}
