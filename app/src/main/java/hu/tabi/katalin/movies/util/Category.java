package hu.tabi.katalin.movies.util;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class Category {

    public static final int PAGE_PEOPLE = 0;
    public static final int PAGE_MOVIES = 1;
    public static final int PAGE_TVSHOWS = 2;
}
