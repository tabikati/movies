package hu.tabi.katalin.movies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import hu.tabi.katalin.movies.model.Genre;
import hu.tabi.katalin.movies.model.Item;
import hu.tabi.katalin.movies.model.MovieDetails;
import hu.tabi.katalin.movies.model.TvShowDetails;
import hu.tabi.katalin.movies.util.AnimationHelper;
import hu.tabi.katalin.movies.util.Category;
import hu.tabi.katalin.movies.util.ConnectionHelper;

public class DetailsActivity extends AppCompatActivity {



    public static final String EXTRA_CATEGORY = "category";
    public static final String EXTRA_ID = "id";

    private TextView mTitleTextView;
    private TextView mGenreTextView;
    private TextView mVoteTextView;
    private TextView mDateTextView;
    private TextView mOverviewTextView;
    private ImageView mPosterImageView;
    private ViewGroup mContentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        findViews();

        int category = getIntent().getIntExtra(EXTRA_CATEGORY, -1);
        int id = getIntent().getIntExtra(EXTRA_ID, -1);

        if (category == Category.PAGE_MOVIES) {
            ConnectionHelper.getMovieDetails(this, id, new ConnectionHelper.FinishListener() {
                @Override
                public void onSuccess(JsonObject jsonObject) {
                    updateLayout(MovieDetails.parse(jsonObject));
                }

                @Override
                public void onError() {
                    finish();
                }
            });

        } else if (category == Category.PAGE_TVSHOWS) {
            ConnectionHelper.getTvDetails(this, id, new ConnectionHelper.FinishListener() {
                @Override
                public void onSuccess(JsonObject jsonObject) {
                    updateLayout(TvShowDetails.parse(jsonObject));
                }

                @Override
                public void onError() {
                    finish();
                }
            });
        }
    }

    private void findViews() {
        mTitleTextView = (TextView) findViewById(R.id.titleTextView);
        mGenreTextView = (TextView) findViewById(R.id.genreTextView);
        mVoteTextView = (TextView) findViewById(R.id.voteTextView);
        mDateTextView = (TextView) findViewById(R.id.dateTextView);
        mOverviewTextView = (TextView) findViewById(R.id.overviewTextView);
        mPosterImageView = (ImageView) findViewById(R.id.posterImageView);
        mContentLayout = (ViewGroup) findViewById(R.id.contentLayout);
        mContentLayout.setAlpha(0);
    }

    private void showContent() {
        AnimationHelper.animateShow(mContentLayout);
    }

    private void updateLayout(MovieDetails movieDetails) {
        showContent();
        mTitleTextView.setText(movieDetails.getTitle());
        String genres = "";
        for (Genre genre : movieDetails.getGenreList()) {
            if (genres.length() > 0) {
                genres += ", ";
            }
            genres += genre.getName();
        }
        mGenreTextView.setText(genres);
        mVoteTextView.setText(movieDetails.getVoteAverage() + "");
        if (movieDetails.getReleaseDate() != null) {
            mDateTextView.setText(new SimpleDateFormat(Item.DATE_FORMAT, Locale.getDefault()).format(movieDetails.getReleaseDate()));
        }
        mOverviewTextView.setText(movieDetails.getOverview());
        ConnectionHelper.loadImage(mPosterImageView, movieDetails.getPosterPath());
    }

    private void updateLayout(TvShowDetails tvShowDetails) {
        showContent();
        mTitleTextView.setText(tvShowDetails.getTitle());
        String genres = "";
        for (Genre genre : tvShowDetails.getGenreList()) {
            if (genres.length() > 0) {
                genres += ", ";
            }
            genres += genre.getName();
        }
        mGenreTextView.setText(genres);
        mVoteTextView.setText(tvShowDetails.getVoteAverage() + "");
        if (tvShowDetails.getReleaseDate() != null) {
            mDateTextView.setText(new SimpleDateFormat(Item.DATE_FORMAT, Locale.getDefault()).format(tvShowDetails.getReleaseDate()));
        }
        mOverviewTextView.setText(tvShowDetails.getOverview());
        ConnectionHelper.loadImage(mPosterImageView, tvShowDetails.getPosterPath());
    }
}
