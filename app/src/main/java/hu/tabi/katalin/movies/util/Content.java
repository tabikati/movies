package hu.tabi.katalin.movies.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.tabi.katalin.movies.model.Genre;
import hu.tabi.katalin.movies.model.Movie;
import hu.tabi.katalin.movies.model.Person;
import hu.tabi.katalin.movies.model.TvShow;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class Content {

    private static Content mInstance;
    private List<Movie> mMovieList;
    private List<Person> mPersonList;
    private List<TvShow> mTvShowList;
    private Map<Integer, String> mMovieGenres;
    private Map<Integer, String> mTvGenres;

    private Content() {
        mMovieList = new ArrayList<>();
        mPersonList = new ArrayList<>();
        mTvShowList = new ArrayList<>();
        mMovieGenres = new HashMap<>();
        mTvGenres = new HashMap<>();
    }

    public static Content getInstance() {
        if (mInstance == null) {
            mInstance = new Content();
        }
        return mInstance;
    }

    public List<Movie> getMovieList() {
        return mMovieList;
    }

    public void setMovieList(List<Movie> movieList) {
        mMovieList = movieList;
    }

    public List<Person> getPersonList() {
        return mPersonList;
    }

    public void setPersonList(List<Person> personList) {
        mPersonList = personList;
    }

    public List<TvShow> getTvShows() {
        return mTvShowList;
    }

    public void setTvShowList(List<TvShow> tvShowList) {
        mTvShowList = tvShowList;
    }

    public void setMovieGenres(List<Genre> movieGenreList) {
        for (Genre movieGenre : movieGenreList) {
            mMovieGenres.put(movieGenre.getId(), movieGenre.getName());
        }
    }

    public String getMovieGenre(int id) {
        return mMovieGenres.get(id);
    }

    public void setTvGenres(List<Genre> tvGenres) {
        for (Genre genre : tvGenres) {
            mTvGenres.put(genre.getId(), genre.getName());
        }
    }

    public String getTvGenre(int id) {
        return mTvGenres.get(id);
    }

}
