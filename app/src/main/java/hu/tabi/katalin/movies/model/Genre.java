package hu.tabi.katalin.movies.model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import hu.tabi.katalin.movies.util.Members;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class Genre {

    @SerializedName("id")
    private int mId;

    @SerializedName("name")
    private String mName;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public static List<Genre> parseList(JsonObject jsonObject) {
        return new Gson().fromJson(jsonObject.getAsJsonArray(Members.GENRES), new TypeToken<List<Genre>>() {
        }.getType());
    }
}
