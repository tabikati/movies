package hu.tabi.katalin.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.movies.fragment.TopListFragment;
import hu.tabi.katalin.movies.model.Genre;
import hu.tabi.katalin.movies.model.Movie;
import hu.tabi.katalin.movies.model.Person;
import hu.tabi.katalin.movies.model.TvShow;
import hu.tabi.katalin.movies.util.AnimationHelper;
import hu.tabi.katalin.movies.util.Category;
import hu.tabi.katalin.movies.util.ConnectionHelper;
import hu.tabi.katalin.movies.util.Content;
import hu.tabi.katalin.movies.util.CustomUtil;
import hu.tabi.katalin.movies.util.Members;

public class MainActivity extends AppCompatActivity {


    private ViewGroup mSearchLayout;
    private EditText mSearchEditText;
    private ImageView mSearchButton;
    private ViewPager mPager;
    private FragmentPagerAdapter mPagerAdapter;

    private List<TopListFragment> mFragmentList;

    private boolean mPopularMoviesDownloaded;
    private boolean mMovieGenresDownloaded;
    private boolean mPopularTvShowsDownloaded;
    private boolean mTvGenresDownloaded;

    private int mCurrentPagePosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createFragments();
        initSearchLayout();
        initPager();
        downloadContent();
    }

    private void initPager() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case Category.PAGE_MOVIES:
                        return getString(R.string.tab_movies);
                    case Category.PAGE_PEOPLE:
                        return getString(R.string.tab_people);
                    case Category.PAGE_TVSHOWS:
                        return getString(R.string.tab_tvshows);
                }
                return "";
            }
        };
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(Category.PAGE_MOVIES);
        mCurrentPagePosition = Category.PAGE_MOVIES;

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                CustomUtil.hideSoftKeyboard(MainActivity.this);
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPagePosition = position;
                String hint = "";
                switch (position) {
                    case Category.PAGE_MOVIES:
                        hint = getString(R.string.search_hint_movies);
                        break;
                    case Category.PAGE_PEOPLE:
                        hint = getString(R.string.search_hint_people);
                        break;
                    case Category.PAGE_TVSHOWS:
                        hint = getString(R.string.search_hint_tvshows);
                        break;
                }
                mSearchEditText.setHint(hint);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
        pagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.white));
        for (int i = 0; i < pagerTabStrip.getChildCount(); i++) {
            if (pagerTabStrip.getChildAt(i) instanceof TextView) {
                ((TextView) pagerTabStrip.getChildAt(i)).setTextColor(getResources().getColor(R.color.white));
                ((TextView) pagerTabStrip.getChildAt(i)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            }
        }
    }

    private void initSearchLayout() {
        mSearchLayout = (ViewGroup) findViewById(R.id.searchLayout);
        mSearchEditText = (EditText) findViewById(R.id.searchEditText);
        mSearchEditText.setHint(getString(R.string.search_hint_movies));
        mSearchButton = (ImageView) findViewById(R.id.searchButtonImageView);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
    }

    private void search() {
        String query = mSearchEditText.getText().toString();
        if (query.length() == 0) {
            return;
        }
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(ResultActivity.EXTRA_CATEGORY, mCurrentPagePosition);
        intent.putExtra(ResultActivity.EXTRA_QUERY, query);
        startActivity(intent);
    }

    public void showSearchLayout() {
        AnimationHelper.animateUp(mSearchLayout);
    }

    public void hideSearchLayout() {
        AnimationHelper.animateDown(mSearchLayout);
    }

    private void createFragments() {
        mFragmentList = new ArrayList<>();
        mFragmentList.add(Category.PAGE_PEOPLE, TopListFragment.newInstance());
        mFragmentList.add(Category.PAGE_MOVIES, TopListFragment.newInstance());
        mFragmentList.add(Category.PAGE_TVSHOWS, TopListFragment.newInstance());
    }

    private void downloadContent() {
        downloadPopularMovies();
        downloadPopularPeople();
        downloadPopularTvShows();
        downloadMovieGenres();
        downloadTvGenres();
    }

    private void downloadPopularMovies() {
        ConnectionHelper.getPopularMovies(this, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                Content.getInstance().setMovieList(Movie.parseMovieList(jsonObject));
                mPopularMoviesDownloaded = true;
                checkMovieContent();
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }

    private void downloadPopularPeople() {
        ConnectionHelper.getPopularPeople(this, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                List<Person> personList = Person.parsePersonList(jsonObject);
                Content.getInstance().setPersonList(personList);
                downloadPeopleDetails();
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }

    private void downloadPopularTvShows() {
        ConnectionHelper.getPopulatTvShows(this, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                Content.getInstance().setTvShowList(TvShow.parseTvShowList(jsonObject));
                mPopularTvShowsDownloaded = true;
                checkTvContent();
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }

    private void downloadMovieGenres() {
        ConnectionHelper.getMovieGenres(this, new ConnectionHelper.FinishListener() {
            @Override
            public void onSuccess(JsonObject jsonObject) {
                Content.getInstance().setMovieGenres(Genre.parseList(jsonObject));
                mMovieGenresDownloaded = true;
                checkMovieContent();
            }

            @Override
            public void onError() {
                mMovieGenresDownloaded = true;
                checkMovieContent();
            }
        });
    }

    private void downloadTvGenres() {
        ConnectionHelper.getTvGenres(this, new ConnectionHelper.FinishListener() {

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Content.getInstance().setTvGenres(Genre.parseList(jsonObject));
                mTvGenresDownloaded = true;
                checkTvContent();
            }

            @Override
            public void onError() {
                mTvGenresDownloaded = true;
                checkTvContent();
            }
        });
    }

    private int mItemsDownloaded = 0;

    private void downloadPeopleDetails() {
        final List<Person> personList = Content.getInstance().getPersonList();
        for (final Person person : personList) {
            ConnectionHelper.getPersonDetails(MainActivity.this, person.getId(), new ConnectionHelper.FinishListener() {
                @Override
                public void onSuccess(JsonObject jsonObject) {
                    person.setDesc(new Gson().fromJson(jsonObject.get(Members.BIO), String.class));
                    mItemsDownloaded++;
                    if (mItemsDownloaded == personList.size()) {
                        checkPeopleContent();
                    }
                }

                @Override
                public void onError() {
                    person.setDesc("");
                    mItemsDownloaded++;
                    if (mItemsDownloaded == personList.size()) {
                        checkPeopleContent();
                    }
                }
            });
        }
    }

    private void checkMovieContent() {
        if (mPopularMoviesDownloaded && mMovieGenresDownloaded) {
            mFragmentList.get(Category.PAGE_MOVIES).update(Content.getInstance().getMovieList());
        }
    }

    private void checkTvContent() {
        if (mPopularTvShowsDownloaded && mTvGenresDownloaded) {
            mFragmentList.get(Category.PAGE_TVSHOWS).update(Content.getInstance().getTvShows());
        }
    }

    private void checkPeopleContent() {
        mFragmentList.get(Category.PAGE_PEOPLE).update(Content.getInstance().getPersonList());
    }

}
