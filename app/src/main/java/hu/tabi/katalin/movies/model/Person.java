package hu.tabi.katalin.movies.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import hu.tabi.katalin.movies.util.Members;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class Person extends Item {

    @SerializedName("id")
    private int mId;

    @SerializedName("name")
    private String mTitle;

    @SerializedName("profile_path")
    private String mImage;

    private String mDesc;


    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String mImage) {
        this.mImage = mImage;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public static List<Person> parsePersonList(JsonObject jsonObject) {
        List<Person> list = null;
        try {
            list = getGson().fromJson(jsonObject.getAsJsonArray(Members.RESULTS), new TypeToken<List<Person>>() {
            }.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return list;
    }
}
