package hu.tabi.katalin.movies.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.movies.MainActivity;
import hu.tabi.katalin.movies.R;
import hu.tabi.katalin.movies.adapter.TopListAdapter;
import hu.tabi.katalin.movies.model.Item;
import hu.tabi.katalin.movies.util.AnimationHelper;

/**
 * Created by Kati on 2016. 10. 16..
 */

public class TopListFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private TopListAdapter mTopListAdapter;

    private List<? extends Item> mItemList = new ArrayList<>();

    private float mAlpha = 0;

    public static TopListFragment newInstance() {
        return new TopListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAlpha(mAlpha);
        mTopListAdapter = new TopListAdapter(getActivity(), mItemList);
        mRecyclerView.setAdapter(mTopListAdapter);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (((LinearLayoutManager) mRecyclerView.getLayoutManager()).findLastVisibleItemPosition() == mItemList.size() - 1) {
                            if (getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).hideSearchLayout();
                            }
                        } else {
                            if (getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).showSearchLayout();
                            }
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity) getActivity()).hideSearchLayout();
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity) getActivity()).hideSearchLayout();
                        }
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        return view;
    }


    public void update(List<? extends Item> itemList) {
        mAlpha = 1;
        mItemList = itemList;
        if (mTopListAdapter != null) {
            mTopListAdapter.update(itemList);
        }
        if (mRecyclerView != null) {
            AnimationHelper.animateShow(mRecyclerView);
        }
    }


}
